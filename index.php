  <?php get_header(); ?>
  <div>
      <div id="ads-left">
        <div id="ddd" style='margin:0 0 5px 0; padding:0;width:200px;position:fixed; left:0; top:186px; z-index:9999;'>        
            <div id="hide_ads" style="width: 27px; float: left;font-size: 20px;background-color: black;text-align: center;color: white;" class="ion-close-circled" data-pack="default" data-tags="delete, trash, kill, x"></div>        
            <div id="show_adsss" style="width: 30px; float: left;font-size: 20px;background-color: #040404;text-align: center;color: white;border-left: 1px solid white;" class="ion-arrow-right-b" data-pack="default" data-tags=""></div>
            <div class="clearfix"></div>
            <a id="abcd" href="http://opennet.com.kh/news/promotion-on-august-2015/">
            </a>        
        </div>
      </div>
  </div>
  <!-- top sec start -->
  <div class="container" style="max-width: 1024px;">
    <div class="row"> 
      <!-- banner outer start -->
      <div class="col-sm-16 banner-outer wow fadeInLeft animated" data-wow-delay="1s" data-wow-offset="50">
        <div class="row">
          <div class="col-sm-16 col-md-16 col-lg-16">            
            <!-- carousel start -->
            <div id="sync1" class="owl-carousel">
            <?php
                $post_homeid = '';
                $args = array( 'posts_per_page' => 5,'meta_key' => 'showcase', 'meta_value' => 1);
                $postslist = get_posts( $args );
                foreach ( $postslist as $post ) :
                ?>
                <div class="box item">
                    <?php 
                        $link = get_the_permalink();
                        $cate = get_the_category(get_the_ID());
                        foreach($cate as $cateddd){
                            if($cateddd->slug == 'slider-banner'){
                                $link = '#';
                                $custom_fields = get_post_custom();
                                $my_custom_field = $custom_fields['LinkTo'];
                                if(isset($my_custom_field)){
                                   foreach ( $my_custom_field as $key => $value ) {
                                        if($value == ''){
                                            
                                        }else{
                                            $link = $value;
                                        }
                                        
                                    } 
                                }
                                
                            }
                        } 
                    ?>
                    <a href="<?php print($link); ?>">
                    <div class="carousel-caption"><?php the_title(); ?></div>                    
                    <?php
                    $image = get_field('image');
                    if( !empty($image) ): ?>                    
                    	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="100%" height="350" />
                        
                    <?php endif; ?>
                    <div class="overlay"></div>
                    <div class="overlay-info">
                      <div class="cat">
                        <p class="cat-data"><span class="ion-flask"></span>
                        <?php 
                        $i=1;
                        foreach($cate as $cateddd){
                            if($i == 1){
                                $dauphay = '';
                            }else{
                                $dauphay = ', ';
                            }
                            echo $dauphay.$cateddd->name;
                            $i++;
                        }
                        ?>
                        </p>
                      </div>
                      <div class="info">
                        <p><span class="ion-android-data"></span><?php the_time('d-m-Y'); ?><span class="ion-chatbubbles"></span><?php print(fetchPostViews(get_the_ID())); ?></p>
                      </div>
                    </div>
                    </a>
                </div>
                
                <?php
                endforeach;
            ?> 
            </div>
            <!--<div class="row">
              <div id="sync2" class="owl-carousel">
                  <?php
                    foreach ( $postslist as $post ) :
                  ?>
                    <div class="item">                        
                         <?php                     
                            $image = get_field('image');                    
                            if( !empty($image) ): ?>
                                <img class="img-responsive" src="<?php echo $image['url']; ?>" width="762" height="360" alt="<?php echo $image['alt']; ?>"/>                    
                            <?php endif;
                         ?>
                    </div>
                  <?php
                    endforeach;
                  ?>
              </div>
            </div>-->
          </div>          
        </div>
      </div>
      <!-- banner outer end -->
    </div>
  </div>
  <!-- top sec end --> 
  <!-- data start -->
  <div class="container" style="max-width: 1024px;">
    <div class="row "> 
      <!-- left sec start -->
      <div class="col-md-11 col-sm-11">
        <div class="row"> 
          <!-- New & Event start -->            
          <div class="col-sm-16 business  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="50">
            <div class="main-title-outer pull-left">
            <?php
            $args = array(
              'orderby' => 'name',
              'parent' => 0
              );
            $categories = get_categories( $args );
            foreach ( $categories as $category ) {
                if($category->slug == 'new-events')
            	echo '
                <div class="main-title">'.$category->name.'</div>
                <div class="span-outer"><span class="pull-right text-danger last-update"><span class="ion-android-data icon"></span><a href="' . get_category_link( $category->term_id ) . '">View all posts</a></span> </div>';
            }
            ?>
            </div>
            <div class="row">
              <div class="col-md-18 col-sm-18">
                <div class="row">
                <?php
                $post_homeid = '';
                $args = array( 'posts_per_page' => 1, 'order'=> 'DESC', 'orderby' => 'post_date' , 'category' => '9');
                $postslist = get_posts( $args );
                foreach ( $postslist as $post ) :
                  setup_postdata( $post ); ?> 
                    <div class="col-md-8 col-sm-9 col-xs-16">
                        <div class="topic">
                            <a href="<?php the_permalink(); ?>">
                            <?php 
                            $post_homeid = $post->ID;
                            if ( has_post_thumbnail() ) { 
                                $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(),"400x300" );
                            	echo '<img class="img-thumbnail" src="'.$large_image_url[0].'" width="400" height="398" alt=""/>';
                            } 
                            ?>
                            <h3><?php the_title(); ?>  </h3>
                            <div class="text-danger sub-info-bordered">
                                <div class="time"><span class="ion-android-data icon"></span><?php the_time('d-m-Y'); ?></div>
                                <div class="comments"><span class="ion-android-contact icon"></span><?php print(fetchPostViews(get_the_ID())); ?></div>
                            </div>
                            
                            <p>
                            <?php
                            the_excerpt_max_charlength(400,$post_homeid);
                            ?>
                            </p>
                            </a>
                        </div>
                    </div>
                <?php
                endforeach; 
                wp_reset_postdata();
                ?>

                  <div class="col-md-8 col-sm-7 col-xs-16">
                    <ul class="list-unstyled">
                    <?php
                        $args = array( 'posts_per_page' => 4, 'order'=> 'DESC', 'orderby' => 'post_date' , 'category' => '9');
                        $postslist = get_posts( $args );
                        foreach ( $postslist as $post ) :
                        if($post->ID != $post_homeid){
                          setup_postdata( $post ); ?> 
                            <li>
                                <a href="<?php the_permalink(); ?>">
                                <div class="row">
                                  <div class="col-sm-5 hidden-sm hidden-md">
                                    <?php 
                                    if ( has_post_thumbnail() ) { 
                                        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id() );
                                    	echo '<img class="img-thumbnail pull-left" src="'.$large_image_url[0].'" width="150" height="76" alt=""/>';
                                    } 
                                    ?>
                                  </div>
                                  <div class="col-sm-16 col-md-16 col-lg-11">
                                  <?php
                                    if(isset($_GET['lang'])) {
                                        if($_GET['lang'] == 'km') { ?>
                                            <h4 style="font-size: 14px; font-weight: bold; word-wrap: break-word;"><?php the_title(); ?></h4>
                                       <?php }else{ ?>
                                            <h4><?php the_title(); ?></h4>
                                       <?php }
                                    }else{ ?>
                                        <h4><?php the_title(); ?></h4>
                                 <?php   }
                                    
                                  ?>
                                    
                                    <div class="text-danger sub-info">
                                      <div class="time"><span class="ion-android-data icon"></span><?php the_time('d-m-Y'); ?></div>
                                      <div class="comments"><span class="ion-android-contact icon"></span><?php print(fetchPostViews(get_the_ID())); ?></div>
                                    </div>
                                    <p style="word-wrap: break-word;font-size: 14px !important;">
                                    <?php
                                        the_excerpt_max_charlength(250,$post->ID);
                                    ?>
                                    </p>
                                  </div>
                                </div>
                                </a>
                            </li>
                        <?php
                        }
                        endforeach; 
                        wp_reset_postdata();
                        ?>
                    </ul>
                  </div>
                </div>
              </div>
              
            </div>
            <hr>
          </div>
          <!-- business end --> 
          
          <!-- Science & Travel start -->
          <div class="col-sm-16">
            <div class="row">
              <div class="col-xs-16 col-sm-8  wow fadeInLeft animated science" data-wow-delay="0.5s" data-wow-offset="130">
                <div class="main-title-outer pull-left">
                  <div class="main-title">Home Premium</div>
                  <div class="span-outer"></div>
                </div>
                <div class="row">
                  <div class="topic col-sm-16"> 
                    <a href="./?page_id=200">
                        <img class=" img-thumbnail" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/home.jpg" width="600" height="227" alt=""/>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-sm-8 col-xs-16 wow fadeInRight animated" data-wow-delay="0.5s" data-wow-offset="130">
                <div class="main-title-outer pull-left">
                  <div class="main-title">Business</div>
                  <div class="span-outer"> </div>
                </div>
                <div class="row left-bordered">
                  <div class="topic col-sm-16">
                    <a href="./?page_id=198">
                        <img class="img-thumbnail" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/business.jpg" width="600" height="227" alt=""/>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <hr/>
          </div>
          
          <!-- Scince & Travel end --> 
          <!-- lifestyle start-->
          <div class="col-sm-16">
            <div class="row">
              <div class="main-title-outer pull-left">
                      <div class="main-title">Promotions</div>
                      <div class="span-outer"> </div>
              </div>
              <?php
                $args = array( 'posts_per_page' => 4, 'order'=> 'DESC', 'orderby' => 'post_date' , 'category' => 10);
                $postslist = get_posts( $args );
                foreach ( $postslist as $post ) :
                    
                ?>
                <div class="col-sm-8 col-xs-16 wow fadeInRight animated" data-wow-delay="0.5s" data-wow-offset="130">
                    <div class="row left-bordered">
                      <div class="topic col-sm-16">
                        <a href="<?php the_permalink(); ?>">
                            <h4>
                                <?php the_title(); ?>
                            </h4>
                            <?php 
                        if ( has_post_thumbnail() ) { 
                            $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), '550' );
                        	echo '<img style="max-height: 118px;" class="img-responsive" src="'.$large_image_url[0].'" width="550" height="300" alt=""/>';
                        } 
                        ?>
                        </a>
                      </div>
                    </div>
                  </div>
                    
                <?php
                endforeach;
                ?>
              
              
            </div>
            <hr/>
          </div>
          <!-- lifestyle end --> 
          
          <!--Recent videos start-->
          <!--Recent videos end--> 
          <!--wide ad start-->
          <div class="col-sm-16 wow fadeInDown animated " data-wow-delay="0.5s" data-wow-offset="25"><img class="img-responsive" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ads/728-90-ad.gif" width="728" height="90" alt=""/></div>
          <!--wide ad end-->
        </div>
      </div>
      <!-- left sec end --> 
      <!-- right sec start -->
      <?php get_sidebar(); ?>
      <!-- right sec end --> 
    </div>
  </div>
  <!-- data end --> 
  
  <!-- Footer start -->
  <?php get_footer(); ?>
  <!-- Footer end -->
</div>
<!-- wrapper end --> 

    <!-- jQuery --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js"></script> 
    <!--jQuery easing--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script> 
    <!-- bootstrab js --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.js"></script> 
    <!--style switcher--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/style-switcher.js"></script> <!--wow animation--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/wow.min.js"></script> 
    <!-- time and date --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/moment.min.js"></script> 
    <!--news ticker--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.ticker.js"></script> 
    <!-- owl carousel --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.js"></script> 
    <!-- magnific popup --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.magnific-popup.js"></script> 
    <!-- weather 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.simpleWeather.min.js"></script> --> 
    <!-- calendar--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.pickmeup.js"></script> 
    <!-- go to top --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.scrollUp.js"></script> 
    <!-- scroll bar  -->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.nicescroll.js"></script> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.nicescroll.plus.js"></script> 
    <!--masonry--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/masonry.pkgd.js"></script> 
    <!--media queries to js--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/enquire.js"></script> 
    <!--custom functions--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/custom-fun.js"></script>
</body>
</html>