<?php
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}
add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
  register_nav_menu( 'primary', 'Primary Menu' );
}

/**
 * View Counter
**/
function observePostViews($postID) {
    $count_key = 'viewcounter';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
function fetchPostViews($postID){
    $count_key = 'viewcounter';
    $count = get_post_meta($postID, $count_key, true);
    if($count == '' ){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
/**
 * Get Image Showcase
 */
function showcaseimage($postID){
    $count_key = 'imageshowcase';
    $count = get_post_meta($postID, $count_key);
    if($count == '' ){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "no image";
    }
    return $count;
}
 
/**
 * Register our sidebars and widgetized areas.
 *
 */

function arphabet_widgets_init() {
	if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name'=> 'Left Tab',
    		'id' => 'left_tab',
    		'before_widget' => '<li id="%1$s" class="widget %2$s">',
    		'after_widget' => '</li>',
    		'before_title' => '<h3>',
    		'after_title' => '</h3>',
    	));
        register_sidebar(array(
    		'name'=> 'Footer Tags',
    		'id' => 'footer_tags',
    		'before_widget' => '<li id="%1$s" class="widget %2$s">',
    		'after_widget' => '</li>'
    	));
        register_sidebar(array(
    		'name'=> 'Right Tab',
    		'id' => 'right_tab',
    		'before_widget' => '<li id="%1$s" class="widget %2$s">',
    		'after_widget' => '</li>',
    		'before_title' => '<h3>',
    		'after_title' => '</h3>',
    	));
        register_sidebar(array(
    		'name'=> 'Language',
    		'id' => 'language_fix_top',
    		'before_widget' => '<div id="%1$s" style="position: fixed; top: 0; float: right; text-align: right; right: 0;
    z-index: 99999;
" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<h3>',
    		'after_title' => '</h3>',
    	));
	register_sidebar(array(
    		'name'=> 'Right Column',
    		'id' => 'right_column',
    		'before_title' => '<h3>',
    		'after_title' => '</h3>',
    	));
    }
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/**
 * add featured image 
 *
 */
add_theme_support('post-thumbnails');

function the_excerpt_max_charlength($charlength,$link) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '<br /><a href="?p='.$link.'" style="color: rgb(237, 117, 0);font-size: 14px !important;">[Read More]</a>';
	} else {
		echo $excerpt;
	}
}
/**
 * add menu
 *
 */
add_theme_support('menu');

?>
<?php
function custom_pagination($numpages = '', $pagerange = '', $paged='') {
 
  if (empty($pagerange)) {
    $pagerange = 2;
  }
 
  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }
 
  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);
  
  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }
 
}
if ( ! function_exists( 'globalnews_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 * Based on paging nav function from Twenty Fourteen
 */

function globalnews_paging_nav() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}

	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );

	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}

	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

	// Set up paginated links.
	$links = paginate_links( array(
		'base'     => $pagenum_link,
		'format'   => $format,
		'total'    => $GLOBALS['wp_query']->max_num_pages,
		'current'  => $paged,
		'mid_size' => 3,
		'add_args' => array_map( 'urlencode', $query_args ),
		'prev_text' => __( '&larr; Previous', 'globalnews' ),
		'next_text' => __( 'Next &rarr;', 'globalnews' ),
		'type'      => 'list',
	) );

	if ( $links ) :

	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'globalnews' ); ?></h1>
			<?php echo $links; ?>
	</nav><!-- .navigation -->
	<?php
	endif;
}
endif;

?>