<div class="col-sm-5 hidden-xs right-sec">
<div class="bordered top-margin">
  <div class="row ">
    <!-- Right Sidebar Top-->
    <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="130">
        <a href="<?php echo site_url(); ?>/?cat=5"><img src="<?php bloginfo('template_directory') ?>/images/qafaq.jpg" width="100%" class="img-responsive" /></a>
    </div>
    <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="130">
    <hr />
          <h3 style="
    background: rgb(237, 237, 237);
    color: rgb(32, 8, 8);
    padding: 8px;
    margin: 0;
    text-align: center;
">Packages</h3>
          <ul class="nav nav-tabs nav-justified " role="tablist">
            <li class="active"><a href="#tabcard" role="tab" data-toggle="tab">ADSL</a></li>
            <li><a href="#recentcard" role="tab" data-toggle="tab">FTTH</a></li>
          </ul>
          
          <!-- Tab panes -->
          <div class="tab-content">
            
            <div class="tab-pane active" id="tabcard">
              <ul class="list-unstyled">
                <li>
                    <select class="form-control" id="goi">
                        <option value="0">Package</option>
                        <option value="1">1. HomeLite</option> 
                        <option value="2">2. HomeBasic</option>
                        <option value="3">3. HomeAdvance</option>
                        <option value="4">4. HomePlus</option>
                    </select>
                    <div style="text-align: center;"><i class="ion-arrow-down-c" data-pack="default" data-tags="" style="font-size: 23px;"></i></div>
                    <select class="form-control" id="pre1"> 
                        <option value="0">Prepaid</option>
                        <option value="1">01 months</option>
                        <option value="3">03 months</option>
                        <option value="5">05 months</option>
                        <option value="6">06 months</option>
                        <option value="10">10 months</option>
                        <option value="12">12 months</option>
                    </select><br />
                    <h4 style="text-align: center;">Information</h4>
                    <!-- 1 -->
                    
                    <input id="goi1" value="12" type="hidden" />
                    <input id="goi2" value="18" type="hidden"/>
                    <input id="goi3" value="24" type="hidden"/>
                    <input id="goi4" value="30" type="hidden"/>
                    <div id="monpre1">
                        <span class="col-sm-16" id="info12"></span>
                        <span class="col-sm-8 text-right">Installation Fee:</span><span id="install" class="col-sm-8 text-left" style="padding-left: 25px;"></span><br />
                        <div class="clearfix"></div>
                        <span class="col-sm-8 text-right">Modem/Wifi Router:</span><span id="wifi" class="col-sm-8 text-left" style="padding-left: 25px;"></span><br />
                        <div class="clearfix"></div>
                        <span id="countmonth" class="col-sm-8 text-right">01 months charge:</span><span id="fee" class="col-sm-8 text-left" style="padding-left: 25px;"></span><br />
                        <div class="clearfix"></div>
                        <span class="col-sm-8 text-right">Total charge:</span><span id="totals" class="col-sm-8 text-left" style="padding-left: 25px;"></span>
                        <div class="clearfix"></div>
                        <span class="col-sm-8 hidden text-right">Total scratch card: </span><span id="totalscard" class="hidden col-sm-8 text-left" style="padding-left: 25px;"></span>
                    </div>                    
                </li>     
              </ul>
            </div>
            <div class="tab-pane" id="recentcard">
              <ul class="list-unstyled">                    
                <li><select class="form-control" id="goibus">
                        <option value="0">Package</option>
                        <option value="1">1. BusinessSave</option>
                        <option value="2">2. BusinessLite</option>
                        <option value="3">3. BusinessBasic</option>
                        <option value="4">4. BusinessAdvance</option>
                        <option value="5">5. BusinessExpert</option>
                    </select>
                    <div style="text-align: center;"><i class="ion-arrow-down-c" data-pack="default" data-tags="" style="font-size: 23px;"></i></div>
                    <select class="form-control" id="prebus"> 
                        <option value="0">Prepaid</option>
                        <option value="1">01 months</option>
                        <option value="3">03 months</option>
                        <option value="6">06 months <b>(Discount 5%)</b></option>
                        <option value="12">12 months</option>
                    </select><br />
                    <h4 style="text-align: center;">Information</h4>
                    <!-- 1 -->
                    <input id="bus1" value="39" type="hidden" />
                    <input id="bus2" value="69" type="hidden"/>
                    <input id="bus3" value="99" type="hidden"/>
                    <input id="bus4" value="149" type="hidden"/>
                    <input id="bus5" value="249" type="hidden"/>
                    <div id="buspre">
                        <span class="col-sm-8 text-right">Installation Fee:</span><span id="installbus" class="col-sm-8 text-left" style="padding-left: 25px;"></span><br />
                        <div class="clearfix"></div>
                        <span class="col-sm-8 text-right">Modem/Wifi Router:</span><span id="wifibus" class="col-sm-8 text-left" style="padding-left: 25px;"></span><br />
                        <div class="clearfix"></div>
                        <span class="col-sm-8 text-right">Optical Network Unit:</span><span id="ONU" class="col-sm-8 text-left" style="padding-left: 25px;"></span><br />
                        <div class="clearfix"></div>
                        <span id="countmonthbus" class="col-sm-8 text-right">01 months charge:</span><span id="feebus" class="col-sm-8 text-left" style="padding-left: 25px;"></span><br />
                        <div class="clearfix"></div>
                        <span class="col-sm-8 text-right">Total charge:</span><span id="totalsbus" class="col-sm-8 text-left" style="padding-left: 25px;"></span>
                        <div class="clearfix"></div>
                        <span class="col-sm-8 hidden  text-right">Total scratch card: </span><span id="totalscardbus" class="hidden col-sm-8 text-left" style="padding-left: 25px;"></span>
                    </div>           
                </li>                   
              </ul>
            </div>
          </div>
          <hr />
    </div>
    <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="130"> 
      <!-- Nav tabs -->
      <img src="<?php bloginfo('template_directory') ?>/images/adslftth.jpg" width="100%" class="img-responsive" />
      <ul class="nav nav-tabs nav-justified " role="tablist">
        <li class="active"><a href="#popular" role="tab" data-toggle="tab">ADSL</a></li>
        <li><a href="#recent" role="tab" data-toggle="tab">FTTH</a></li>
      </ul>
      
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="popular">
          <ul class="list-unstyled">                    
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Left Tab')) : ?>
            <?php endif; ?>                    
          </ul>
        </div>
        <div class="tab-pane" id="recent">
          <ul class="list-unstyled">                    
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Right Tab')) : ?>
            <?php endif; ?>                    
          </ul>
        </div>
      </div>
    </div>
    <!-- End Right Sidebar Top-->
    <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50">
    <hr />
        <a href="<?php echo site_url(); ?>/?p=398">
            <img class="img-responsive" src="<?php bloginfo('template_directory') ?>/images/compare.jpg" width="100%" />
        </a>
    </div>    
    <!-- Facebook Like Box -->
    <div class="col-sm-16  bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50">
    <hr />
    <img class="img-responsive" src="<?php bloginfo('template_directory') ?>/images/facebook.png" width="100%" />  
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "http://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="fb-like-box" data-href="https://www.facebook.com/opennetsupport" data-width="100%" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
    </div>
    <!-- Facebook Like Box --> 
    <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50">
        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Right Column')) : ?>
        <?php endif; ?>
    </div>
    
  </div>
</div>
</div>