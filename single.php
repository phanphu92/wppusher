  <?php get_header(); ?>  
    <!-- bage header Start -->
    <?php 
    if(have_posts()) : while(have_posts()) : the_post();
    $id = get_the_ID();
    observePostViews(get_the_ID());
    
    $categories = get_the_category($id);
    foreach($categories as $cats){
        $perlink = $cats->taxonomy.'/'.$cats->slug.'/';
        $namecate = $cats->name;
        $idcat = $cats->cat_ID;
    }
    $url = home_url( '/' );
    ?>
  <div class="container">
    <div class="page-header">
      <h1></h1>
      <ol class="breadcrumb" style="float: left !important;">
        <li><a href="<?php echo $url; ?>">Home</a></li>
        <li><a href="<?php echo $url.$perlink; ?>"><?php echo $namecate; ?></a></li>
        <li class="active"><?php the_title(); ?></li>
      </ol>
    </div>
  </div>
  <!-- bage header End --> 
  <!-- data start -->
  <div class="container ">
    <div class="row "> 
      <!-- left sec start -->
      <div class="col-md-11 col-sm-11">
        <div class="row"> 
          <!-- post details start -->
            <div class="col-sm-16">
              <div class="row">
              <!-- post info start -->
              
                <div class="sec-topic col-sm-16  wow fadeInDown animated " data-wow-delay="0.5s">
                  <div class="row">                 
                    <div class="col-sm-16">
                    <?php
                    /*if ( has_post_thumbnail() ) { 
                        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(),"400x300" );
                    	echo '<img width="1000" height="606" alt="" src="'.$large_image_url[0].'" class="img-thumbnail" />';
                    }*/
                    ?>
                    
                    </div>
                    <div class="col-sm-16 sec-info">
                      <h3><?php the_title(); ?></h3>
                      <div class="text-danger sub-info-bordered">                  
                        <div class="time"><span class="ion-android-data icon"></span><?php the_time('d-m-Y'); ?></div>
                        <div class="comments"><span class="ion-android-contact icon"></span><?php print(fetchPostViews(get_the_ID())); ?></div>
                      </div>
                      <p>
                      <?php the_content(); ?>
                      </p>
                      <hr />
                    </div>
                  </div>
                </div>
                
                <!-- end post info -->
                <hr />
                <?php if($namecate != '') : ?>
                <div class="col-sm-16 related">
                  <div class="main-title-outer pull-left">
                    <div class="main-title">related <?php echo $namecate; ?></div>
                  </div>
                  <div class="row">
                  <?php
                    $post_homeid = '';
                    $args = array( 'posts_per_page' => 4, 'orderby' => 'rand' , 'category' => ''.$idcat.'');
                    $postslist = get_posts( $args );
                    foreach ( $postslist as $post ) :
                    setup_postdata( $post );
                    if ( has_post_thumbnail() ) { 
                        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(),array( 150,150 ) );
                        $linkrelated = $large_image_url[0];
                    }else{
                        $linkrelated = 'http://placehold.it/230x230';
                    }
                  ?>
                  <div class="item topic col-sm-4 col-xs-16" style="margin-right: 0 !important;">
                      <a href="<?php the_permalink(); ?>"> <img class="img-thumbnail" src="<?php echo $linkrelated; ?>" width="1000" height="606" alt=""/>
                      <h4 style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"><?php the_title(); ?></h4>
                      <div class="text-danger sub-info-bordered remove-borders">
                        <div class="time"><span class="ion-android-data icon"></span><?php the_time('d-m-Y'); ?></div>
                        <div class="comments"><span class="ion-android-contact icon"></span><?php print(fetchPostViews(get_the_ID())); ?></div>
                      </div>
                      </a>
                    </div>
                  <?php                  
                    endforeach;
                  ?>
                  </div>
                </div>
                <?php endif; ?>
              </div>
            </div>
            <!-- post details end --> 
          <div class="col-sm-16 wow fadeInDown animated " data-wow-delay="0.5s" data-wow-offset="25"><img class="img-responsive" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ads/728-90-ad.gif" width="728" height="90" alt=""/></div>
          <!--wide ad end-->
        </div>
      </div>
      <!-- left sec end --> 
      <!-- right sec start -->
      <?php get_sidebar(); ?>
      <!-- right sec end --> 
    </div>
  </div>
  <!-- data end --> 
    <?php 
    endwhile;
    else :
    echo wpautop( 'Sorry, no posts were found' );
    endif;
    ?>
  <!-- Footer start -->
  <?php get_footer(); ?>
  <!-- Footer end -->
</div>
<!-- wrapper end --> 

    <!-- jQuery --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js"></script> 
    <!--jQuery easing--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script> 
    <!-- bootstrab js --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.js"></script> 
    <!--style switcher--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/style-switcher.js"></script> <!--wow animation--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/wow.min.js"></script> 
    <!-- time and date --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/moment.min.js"></script> 
    <!--news ticker--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.ticker.js"></script> 
    <!-- owl carousel --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.js"></script> 
    <!-- magnific popup --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.magnific-popup.js"></script> 
    <!-- weather --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.simpleWeather.min.js"></script> 
    <!-- calendar--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.pickmeup.js"></script> 
    <!-- go to top --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.scrollUp.js"></script> 
    <!-- scroll bar --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.nicescroll.js"></script> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.nicescroll.plus.js"></script> 
    <!--masonry--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/masonry.pkgd.js"></script> 
    <!--media queries to js--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/enquire.js"></script> 
    
    <!--  Slide in post -->
    
    <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.elastislide.js"></script>
    <!--custom functions--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/custom-fun.js"></script>
    <script type="text/javascript">
			
			// example how to integrate with a previewer

			var current = 0,
				$preview = $( '#preview' ),
				$carouselEl = $( '#carousel' ),
				$carouselItems = $carouselEl.children(),
				carousel = $carouselEl.elastislide( {
					current : current,
					minItems : 4,
					onClick : function( el, pos, evt ) {

						changeImage( el, pos );
						evt.preventDefault();

					},
					onReady : function() {

						changeImage( $carouselItems.eq( current ), current );
						
					}
				} );

			function changeImage( el, pos ) {

				$preview.attr( 'src', el.data( 'preview' ) );
				$carouselItems.removeClass( 'current-img' );
				el.addClass( 'current-img' );
				carousel.setCurrent( pos );

			}
            $('#myTab a').click(function (e) {
            	 e.preventDefault();
            	 $(this).tab('show');
            });
            
            $(function () {
                $('#myTab a:last').tab('show');
            })
		</script>
    
</body>
</html>