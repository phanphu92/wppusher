  <?php get_header(); ?>

  <!-- data start -->
  <div class="container"> 
  <div class="page-header">
      <h1>Search Result Found </h1>
  </div>
  </div>
  <div class="container ">
    <div class="row "> 
      <!-- left sec start -->
        <div class="col-md-11 col-sm-11">
          <div class="row">
            <div class="col-sm-16">
            <?php
            global $wp_query;
            $total_results = $wp_query->found_posts;
            ?>
              <h3><?php printf( __( 'We found <span class="text-danger">'.$total_results.'</span> result for: %s' ), get_search_query() ); ?></h3>
              <hr>
            </div>
          <?php           
                
                if ( have_posts() ) : 
                while ( have_posts() ) : the_post();
                $var = the_category_ID(false);
                if($var != 11 && (!is_page())) :
          ?>            
               <div class="sec-topic col-sm-16 wow fadeInDown animated " data-wow-delay="0.5s">
                  <div class="row">
                    <div class="col-sm-7">
                    <?php 
                    $post_homeid = $post->ID;
                    if ( has_post_thumbnail() ) { 
                        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), array(300,300));
                    	echo '<img width="400" height="300" alt="" src="'.$large_image_url[0].'" class="img-thumbnail">';
                    }else{?>
                        <img width="400" height="300" alt="" src=" <?php bloginfo( 'template_directory' ); ?>/images/no-image-available.jpg" class="img-thumbnail" />
                    <?php } 
                    ?>
                    </div>
                    <div class="col-sm-9"> <a href="<?php the_permalink(); ?>">
                      <div class="sec-info">
                        <h3 style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><?php the_title(); ?></h3>
                        <div class="text-danger sub-info-bordered">
                          <div class="time"><span class="ion-android-data icon"></span><?php the_time('d-m-Y'); ?></div>
                          <div class="comments"><span class="ion-android-contact icon"></span><?php print(fetchPostViews(get_the_ID())); ?></div>
                          <!--<div class="stars"><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star-half"></span></div>-->
                        </div>
                      </div>
                      </a>
                      <p style="word-wrap: break-word;"><?php the_excerpt_max_charlength(300,$post_homeid); ?></p>
                    </div>
                  </div>
                </div>  
          <?php
                endif;
              endwhile;
              
          ?>
            
            
            <div class="col-sm-16">
              <hr>
                <?php
                the_posts_pagination( array(
    				'prev_text'          => __( '«' ),
    				'next_text'          => __( '»' ),
    				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '') . ' </span>',
    			) );
                ?>
            </div>
            <?php
                else:
                ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php
                endif;
                
            ?>
          </div>
        </div>
        <!-- left sec end --> 
      <!-- right sec start -->
      <?php get_sidebar(); ?>
      <!-- right sec end --> 
    </div>
  </div>
  <!-- data end --> 
  
  <!-- Footer start -->
  <?php get_footer(); ?>
  <!-- Footer end -->
</div>
<!-- wrapper end --> 

    <!-- jQuery --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js"></script> 
    <!--jQuery easing--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script> 
    <!-- bootstrab js --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.js"></script> 
    <!--style switcher--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/style-switcher.js"></script> <!--wow animation--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/wow.min.js"></script> 
    <!-- time and date --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/moment.min.js"></script> 
    <!--news ticker--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.ticker.js"></script> 
    <!-- owl carousel --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.js"></script> 
    <!-- magnific popup --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.magnific-popup.js"></script> 
    <!-- weather 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.simpleWeather.min.js"></script> --> 
    <!-- calendar--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.pickmeup.js"></script> 
    <!-- go to top --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.scrollUp.js"></script> 
    <!-- scroll bar  -->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.nicescroll.js"></script> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.nicescroll.plus.js"></script> 
    <!--masonry--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/masonry.pkgd.js"></script> 
    <!--media queries to js--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/enquire.js"></script> 
    <!--custom functions--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/custom-fun.js"></script>
</body>
</html>