  <?php get_header(); ?>

  <!-- data start -->
  <div class="container ">
    <div class="row "> 
      <!-- left sec start -->
        <div class="col-md-11 col-sm-11">
          <div class="row">
          <?php
                $cur_cat_id = get_cat_id( single_cat_title("",false) );
                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;   
                if($cur_cat_id != 3 && $cur_cat_id != 5) {
                $custom_args = array(
                  'post_type' => 'post',
                  'posts_per_page' => 8,
                  'paged' => $paged,
                  'cat' => $cur_cat_id
                );
                
                $custom_query = new WP_Query( $custom_args );
                
                if ( $custom_query->have_posts() ) : 
                while ( $custom_query->have_posts() ) : $custom_query->the_post();
          ?>            
               <div class="sec-topic col-sm-16 wow fadeInDown animated " data-wow-delay="0.5s">
                  <div class="row">
                    <div class="col-sm-7">
                    <?php 
                    $post_homeid = $post->ID;
                    if ( has_post_thumbnail() ) { 
                        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), array(300,300));
                    	echo '<img width="400" height="300" alt="" src="'.$large_image_url[0].'" class="img-thumbnail">';
                    } 
                    ?>
                    </div>
                    <div class="col-sm-9"> <a href="<?php the_permalink(); ?>">
                      <div class="sec-info">
                        <h3 style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><?php the_title(); ?></h3>
                        <div class="text-danger sub-info-bordered">
                          <div class="time"><span class="ion-android-data icon"></span><?php the_time('d-m-Y'); ?></div>
                          <div class="comments"><span class="ion-android-contact icon"></span><?php print(fetchPostViews(get_the_ID())); ?></div>
                          <!--<div class="stars"><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star-half"></span></div>-->
                        </div>
                      </div>
                      </a>
                      <p style="word-wrap: break-word;"><?php the_excerpt_max_charlength(300,$post_homeid); ?></p>
                    </div>
                  </div>
                </div>  
          <?php 
              endwhile;
              
          ?>
            
            
            <div class="col-sm-16">
              <hr>
                <?php
                the_posts_pagination( array(
    				'prev_text'          => __( '«' ),
    				'next_text'          => __( '»' ),
    				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '') . ' </span>',
    			) );
                ?>
            </div>
            <?php
                else:
                ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php
                endif;
                }else if($cur_cat_id == 5){
                    if(have_posts()) :
                    ?>
                    <div class="accordion" id="accordion2">
                    <?php
                        while(have_posts()) : the_post();
                        ?>                      
                        
                        	<div class="accordion-group" style="
    padding-bottom: 5px;
">
                        		<div class="accordion-heading" style="
    background: rgba(200, 200, 200, 0.28);
    padding: 12px;
    font-size: 18px;
">
                        			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php the_ID(); ?>" style="
    width: 100%;
    display: block;
    color: rgb(255, 142, 62);
    font-weight: bolder;
">
                        				<?php the_title(); ?>
                        			</a>
                        		</div>
                        		<div id="collapse<?php the_ID(); ?>" class="accordion-body collapse">
                        			<div class="accordion-inner" style="
    padding: 10px;
">
                        				<?php the_content(); ?>
                        			</div>
                        		</div>
                        	</div>
                        <?php
                        endwhile;
                        ?>
                        </div>
                        <?php
                    endif;
                }else {
                    $custom_args = array(
                      'post_type' => 'post',
                      'posts_per_page' => 8,
                      'paged' => $paged,
                      'cat' => $cur_cat_id
                    );
                    
                    $custom_query = new WP_Query( $custom_args );
                    
                    if ( $custom_query->have_posts() ) : 
                    while ( $custom_query->have_posts() ) : $custom_query->the_post();   
                      $custom_fields = get_post_custom();
                      $my_custom_field = $custom_fields['LinkTo'];
                      
                      $post_homeid = $post->ID;
                      if ( has_post_thumbnail() ) { 
                            $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), array(300,300));                        	
                      } 
                        
                      foreach ( $my_custom_field as $key => $value ) {
                        echo '<a href="'.$value.'">';
                      }       
                    ?>
                    <div class="col-sm-8 col-md-8">
                    <h3><?php the_title(); ?></h3>
                    <img class="img-responsive" src="<?php echo $large_image_url[0]; ?>" />
                    </div>
                    </a>
                    <?php
                    endwhile;
                    endif;
                    }
            ?>
          </div>
        </div>
        <!-- left sec end --> 
      <!-- right sec start -->
      <?php get_sidebar(); ?>
      <!-- right sec end --> 
    </div>
  </div>
  <!-- data end --> 
  
  <!-- Footer start -->
  <?php get_footer(); ?>
  <!-- Footer end -->
</div>
<!-- wrapper end --> 

    <!-- jQuery --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js"></script> 
    <!--jQuery easing--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script> 
    <!-- bootstrab js --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.js"></script> 
    <!--style switcher--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/style-switcher.js"></script> <!--wow animation--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/wow.min.js"></script> 
    <!-- time and date --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/moment.min.js"></script> 
    <!--news ticker--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.ticker.js"></script> 
    <!-- owl carousel --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.js"></script> 
    <!-- magnific popup --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.magnific-popup.js"></script> 
    <!-- weather 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.simpleWeather.min.js"></script> --> 
    <!-- calendar--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.pickmeup.js"></script> 
    <!-- go to top --> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.scrollUp.js"></script> 
    <!-- scroll bar  -->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.nicescroll.js"></script> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.nicescroll.plus.js"></script> 
    <!--masonry--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/masonry.pkgd.js"></script> 
    <!--media queries to js--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/enquire.js"></script> 
    <!--custom functions--> 
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/custom-fun.js"></script>
</body>
</html>