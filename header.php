<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title><?php bloginfo('name'); ?></title>
    <link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.ico" type="image/x-icon"/>
    <!-- bootstrap styles-->
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
    <!-- google font 
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'/>-->
    <!-- ionicons font -->
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/ionicons.min.css" rel="stylesheet"/>
    <!-- animation styles -->
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/animate.css" />
    
    <!-- custom styles -->
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/custom-red.css" rel="stylesheet" id="style"/>
    <!-- owl carousel styles-->
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.transitions.css"/>
    <!-- magnific popup styles -->
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/magnific-popup.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/custom.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/elastislide.css"/>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/modernizr.custom.17475.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
</head>
<body>

<!-- preloader start 
<div id="preloader">
  <div id="status"></div>
</div>-->
<!-- preloader end -->
<!-- /END THEME SWITCHER--> <!-- wrapper start -->

<div class="wrapper">
  <!-- header start -->
  <div class="nav-fix-top">
    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Language')) : ?>
    <?php endif; ?>
  </div>
  <div class="container header">
    <div class="row">    
      <div class="col-sm-5 col-md-5 wow fadeInUpLeft animated"><a class="navbar-brand" href="<?php bloginfo('home'); ?>">Opennet</a></div>
      <div class="hidden-xs  col-sm-11 col-md-11 wow fadeInUpLeft animated"><img style="float: right; height: 83px;" class="img-responsive" src="<?php echo site_url(); ?>/wp-content/themes/globalnews/images/output_N0nWFS.gif" /></div>
    </div>
  </div>
  <!-- header end --> 
  <!-- nav and search start -->
  <div class="nav-search-outer"> 
    <!-- nav start -->    
    <nav class="navbar navbar-inverse" role="navigation">
      <div class="container">
        <div class="row">
          <div class="col-sm-16"> <a href="javascript:;" class="toggle-search pull-right"><span class="ion-ios7-search"></span></a>
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <?php
                $defaults = array(
                	'theme_location'  => 'primary',
                	'menu'            => '',
                	'container'       => 'div',
                	'container_class' => '',
                	'container_id'    => '',
                	'menu_class'      => 'menu',
                	'menu_id'         => '',
                	'echo'            => true,
                	'fallback_cb'     => 'wp_page_menu',
                	'before'          => '',
                	'after'           => '',
                	'link_before'     => '',
                	'link_after'      => '',
                	'items_wrap'      => '<ul class="nav navbar-nav text-uppercase main-nav">%3$s</ul>',
                	'depth'           => 0,
                	'walker'          => ''
                );                
                wp_nav_menu( $defaults );                
                ?>
            </div>
          </div>
        </div>
      </div>
      <!-- nav end --> 
      <!-- search start -->
      
      <div class="search-container ">
        <div class="container">
          <?php
            get_search_form();
          ?>
        </div>
      </div>
      <!-- search end --> 
    </nav>
    <!--nav end--> 
  </div>
  
  <!-- nav and search end-->