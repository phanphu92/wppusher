﻿<footer>
<div class="top-sec">
  <div class="container" style="max-width: 1024px;">
    <div class="row match-height-container">
      <div class="col-sm-6 subscribe-info wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
        <div class="row">
          <div class="col-sm-16">
            <div class="f-title">Opennet</div>
            <p>King Technologies LTD. operates telecommunication services based on cable such as copper cable (ADSL, Broadband,etc..) and fiber cable for premium service such as FTTH , Leased Line, etc... supporting the very latest in multimedia and internet services with the highest technology over the world. Its extensive nationwide network coverage is available in all 03 provinces in Cambodia (Phnom Penh, Kandal, Kampong Cham). The company’s workforce consists of more than 100 people including local and foreign experts. Clients are individuals, businesses, research and educational organizations, NGOs,  and departments of the Royal Government of Cambodia.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-5 popular-tags  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
        <div class="f-title">popular tags</div>
        <?php
        $arraya = array(
                    'size' => 'true',
                    'smallest' => 11,
                    'largest' => 22,
                    'unit' => 'pt',
                    'color' => 'true',
                    'maxcolor' => '#000000',
                    'mincolor' => '#CCCCCC',
                    'number' => 45,
                    'format' => 'list',
                    'cloud_selection' => 'count-desc',
                    'cloud_sort' => 'random',
                );
        st_tag_cloud($arraya);
        ?>
      </div>
      <div class="col-sm-5 recent-posts  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
        <div class="f-title">recent posts</div>
        <ul class="list-unstyled">
        <?php
                $post_homeid = '';
                $args = array( 'posts_per_page' => 3, 'order'=> 'DESC', 'orderby' => 'post_date', 'category' => 9);
                $postslist = get_posts( $args );
                foreach ( $postslist as $post ) :
                  setup_postdata( $post );
        ?>
        <li> <a href="<?php the_permalink(); ?>">
            <div class="row">
              <div class="col-sm-4">
                <?php 
                if ( has_post_thumbnail() ) { 
                    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(),array(150,150) );
                    echo '<img class="img-thumbnail pull-left" src="'.$large_image_url[0].'" width="70" height="70" alt=""/>';
                } 
                ?>
                
              </div>
              <div class="col-sm-12">
                <h4><?php the_title(); ?></h4>
                <div class="f-sub-info">
                  <div class="time"><span class="ion-android-data icon"></span><?php the_time('d-m-Y'); ?></div>
                  <div class="comments"><span class="ion-chatbubbles icon"></span><?php comments_number('0 comment','1 comment','% comment'); ?></div>
                </div>
              </div>
            </div>
            </a>
          </li>
        <?php
                endforeach;
        ?>
        </ul>
      </div>
      <!-- End footer recent posts -->
    </div>
  </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61307297-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
var LHCChatOptions = {};
LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500,domain:'opennet.com.kh/news'};
(function() {
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
var refferer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
po.src = 'http://opennet.com.kh/livechat/index.php/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(check_operator_messages)/true/(top)/350/(units)/pixels/(leaveamessage)/true?r='+refferer+'&l='+location;
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>
</footer>
  <?php 
    $args = array( 'posts_per_page' => 1,'meta_key' => 'ispopup', 'meta_value' => 1, 'orderby' => 'post_date');
    $postslist = get_posts( $args );
    foreach ( $postslist as $post ) :
        
    endforeach;
  ?>
  <?php
    if(is_home())
    {
        $i = 0;
        $b = $_SERVER['HTTP_REFERER'];
        if($i == 0) :
        if($b == ''){
            $i++; 
        
    ?>
    <div class="" id="popupContainerFixed">
    <i class="icon ion-android-cancel"></i>
        <div id="popupContainer">   
           <div id="popup" style="width: 50%; text-align: start; background: white !important;">
             <?php                
                $args = array( 'posts_per_page' => 1,'meta_key' => 'ispopup', 'meta_value' => 1, 'orderby' => 'post_date');
                $postslist = get_posts( $args );
                foreach ( $postslist as $post ) :
                setup_postdata( $post );
                ?>
                    <h3 style="text-align: center;background: rgb(245, 140, 20);padding: 5px; color: white;">
                        <?php the_title(); ?>
                    </h3>
                    <div style="
    padding: 0 35px;
">
                        <?php
                            the_content();
                        ?>
                    </div>
                <?php
                endforeach;                    
             ?>
            </div>
        </div>
    </div>
     <?php
     }
     endif;
    }
   ?>